<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/table', [AuthController::class, 'table']);

Route::get('/data-tables', function(){
    return view('data-tables');
});

//CRUD Table Cast
//Create data
//Mengarah ke form tambah data
Route::get('/cast/create', [CastController::class, 'create']);
//Menyimpan data ke database tabel cast
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Mengambil semua data di database
Route::get('/cast', [CastController::class, 'index']);
//detail cast berdasaran id
Route::get('/cast/{cast_id}', [	CastController::class, 'show']);

//Update Data
//Mengarah ke form edit data cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Update berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete Data
//Mengarah ke form edit data cast
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);